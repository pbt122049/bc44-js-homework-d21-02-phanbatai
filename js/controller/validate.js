var showMessage = (id, message) => {
  document.getElementById(id).innerHTML = message;
  document.getElementById(id).style.display = "inherit";
};

var checkFill = (idErr, value) => {
  if (value.length == 0) {
    showMessage(idErr, "Mục này không được để trống");
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};

var checkExists = (account, staffList) => {
  var index = staffList.findIndex((item) => {
    return account == item.account;
  });
  if (index == -1) {
    showMessage("tbTKNV", "");
    return true;
  }
  showMessage("tbTKNV", "Tài khoản đã tồn tại");
};

var checkAccount = (account) => {
  if (!checkFill("tbTKNV", account)) return;
  if (!/^[a-zA-Z0-9]+$/.test(account)) {
    showMessage("tbTKNV", "Tài khoản chỉ bao gồm chữ không dấu và số");
    return false;
  }
  if (account.length < 4 || account.length > 6) {
    showMessage("tbTKNV", "Tài khoản phải dài từ 4 đến 6 ký tự");
    return false;
  }
  showMessage("tbTKNV", "");
  return true;
};

var checkName = (name) => {
  if (!checkFill("tbTen", name)) return;

  if (!/^[a-zA-Z\s]+$/.test(name)) {
    showMessage("tbTen", "Họ tên chỉ bao gồm chữ");
    return false;
  }

  showMessage("tbTen", "");
  return true;
};

var checkEmail = (email) => {
  if (!checkFill("tbEmail", email)) return;

  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (!re.test(email)) {
    showMessage("tbEmail", "Email không hợp lệ");
    return true;
  }

  showMessage("tbEmail", "");
  return true;
};

var checkPass = (pass) => {
  if (!checkFill("tbMatKhau", pass)) return;

  if (pass.length < 6 || pass.length > 10) {
    showMessage("tbMatKhau", "Mật khẩu phải từ 6-10 ký tự");
    return false;
  }

  if (!/\d/.test(pass)) {
    showMessage("tbMatKhau", "Mật khẩu phải chứa ít nhất 1 ký tự số");
    return false;
  }

  if (!/[A-Z]/.test(pass)) {
    showMessage("tbMatKhau", "Mật khẩu phải chứa ít nhất 1 ký tự in hoa");
    return false;
  }

  if (!/[\W_]/.test(pass)) {
    showMessage("tbMatKhau", "Mật khẩu phải chứa ít nhất 1 ký tự đặc biệt");
    return false;
  }

  showMessage("tbMatKhau", "");
  return true;
};

var checkDayWork = (dayWork) => {
  if (!checkFill("tbNgay", dayWork)) return;
  var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

  if (!re.test(dayWork)) {
    showMessage("tbNgay", "Ngày làm không hợp lệ");
    return false;
  }

  // Kiểm tra tính hợp lệ của ngày
  var parts = dayWork.split("/");
  var month = parseInt(parts[0], 10);
  var day = parseInt(parts[1], 10);
  var year = parseInt(parts[2], 10);

  if (year < 1000 || year > 3000 || month == 0 || month > 12 || day <= 0) {
    showMessage("tbNgay", "Ngày làm không hợp lệ");
    return false;
  }

  var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
    monthLength[1] = 29;
  }
  showMessage("tbNgay", "");
  return day > 0 && day <= monthLength[month - 1];
};

var checkBaseSalary = (baseSalary) => {
  if (!checkFill("tbLuongCB", baseSalary)) return;

  if (baseSalary < 1000000 || baseSalary > 20000000) {
    showMessage("tbLuongCB", "Lương cơ bản từ 1.000.000 đến 20.000.000 đ");
    return false;
  }

  showMessage("tbLuongCB", "");
  return true;
};

var checkPosition = (position) => {
  if (position == "Chọn chức vụ") {
    showMessage("tbChucVu", "Vui lòng chọn chức vụ");
    return false;
  }

  showMessage("tbChucVu", "");
  return true;
};

var checkHourWork = (hourWork) => {
  if (!checkFill("tbGiolam", hourWork)) return;
  // Kiểm tra kiểu dữ liệu của giá trị nhập vào
  if (typeof hourWork !== "number" && typeof hourWork !== "string") {
    showMessage("tbGiolam", "Giờ làm phải là số");
    return false;
  }

  if (hourWork < 80 || hourWork > 200) {
    showMessage("tbGiolam", "Giờ làm trong tháng từ 80 đến 200 giờ");
    return false;
  }

  // Kiểm tra giá trị nhập vào có phải là số hay không
  showMessage("tbGiolam", "");
  return !isNaN(hourWork);
};
