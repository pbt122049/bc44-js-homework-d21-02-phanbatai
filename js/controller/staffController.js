function getInfo() {
  var account = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var pass = document.getElementById("password").value;
  var dayWork = document.getElementById("datepicker").value;
  var baseSalary = document.getElementById("luongCB").value;
  var position = document.getElementById("chucvu").value;
  var hourWork = document.getElementById("gioLam").value;

  return new STAFF(
    account,
    name,
    email,
    pass,
    dayWork,
    baseSalary,
    position,
    hourWork
  );
}

function showInfo(staff) {
  document.getElementById("tknv").value = staff.account;
  document.getElementById("name").value = staff.name;
  document.getElementById("email").value = staff.email;
  document.getElementById("password").value = staff.pass;
  document.getElementById("datepicker").value = staff.dayWork;
  document.getElementById("luongCB").value = staff.baseSalary;
  document.getElementById("chucvu").value = staff.position;
  document.getElementById("gioLam").value = staff.hourWork;
}

function renderStaff(staffList){
    var table = document.getElementById("tableDanhSach")
    var contentHTML = ""
    for (var i = 0; i < staffList.length; i++) {
        var staff = staffList[i];
        var contentTr = `<tr>
                            <td>${staff.account}</td>
                            <td>${staff.name}</td>
                            <td>${staff.email}</td>
                            <td>${staff.dayWork}</td>
                            <td>${staff.position}</td>
                            <td>${staff.calcSalary().toLocaleString()}</td>
                            <td>${staff.rating()}</td>
                            <td class="d-flex">
                                <button onclick='modifyStaff("${staff.account}")' class="btn btn-dark" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i></button>
                                <button onclick='deleteStaff("${staff.account}")' class="btn btn-danger ml-2" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>`;
        contentHTML += contentTr;
      }
      table.innerHTML = contentHTML;
}