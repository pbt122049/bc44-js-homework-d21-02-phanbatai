function STAFF(
  account,
  name,
  email,
  pass,
  dayWork,
  baseSalary,
  position,
  hourWork
) {
  this.account = account;
  this.name = name;
  this.email = email;
  this.pass = pass;
  this.dayWork = dayWork;
  this.baseSalary = baseSalary;
  this.position = position;
  this.hourWork = hourWork;
  this.calcSalary = function () {
    if (this.position == "Sếp") {
      return this.baseSalary * 3;
    } else if (this.position == "Trưởng phòng") {
      return this.baseSalary * 2;
    } else {
      return this.baseSalary * 1;
    }
  };
  this.rating = function () {
    if (this.hourWork < 160) {
      return "Trung bình";
    } else if (this.hourWork < 176) {
      return "Khá";
    } else if (this.hourWork < 192) {
      return "Giỏi";
    } else {
      return "Xuất sắc";
    }
  };
}
