//toggle sidebar
$(document).ready(function () {
  $("#sidebarCollapse").on("click", function () {
    $("#sidebar").toggleClass("active");
  });
});

////////////////////////////////
var btnOpen = document.getElementById("btnThem");
var btnCreate = document.getElementById("btnThemNV");
var btnUpdate = document.getElementById("btnCapNhat");
var searchBox = document.getElementById("searchName");

var staffList = [];

var localJson = localStorage.getItem("STAFF_LOCAL");

if (localJson != null) {
  var localList = JSON.parse(localJson);

  for (var i = 0; i < localList.length; i++) {
    var row = localList[i];
    var staffLocal = new STAFF(
      row.account,
      row.name,
      row.email,
      row.pass,
      row.dayWork,
      row.baseSalary,
      row.position,
      row.hourWork
    );
    staffList.push(staffLocal);
    //render
    renderStaff(staffList);
  }
}

btnOpen.addEventListener("click", openCreateModal);
btnCreate.addEventListener("click", createStaff);
btnUpdate.addEventListener("click", updateStaff);

function openCreateModal() {
  btnCreate.style.display = "inherit";
  btnUpdate.style.display = "none";
  resetForm();
}

function createStaff() {
  var staff = getInfo();
  var isValid =
    checkExists(staff.account, staffList) &&
    checkAccount(staff.account) &
      checkName(staff.name) &
      checkEmail(staff.email) &
      checkPass(staff.pass) &
      checkDayWork(staff.dayWork) &
      checkBaseSalary(staff.baseSalary) &
      checkPosition(staff.position) &
      checkHourWork(staff.hourWork);

  if (isValid) {
    resetForm();
    $("#myModal").modal("hide");

    staffList.push(staff);

    var staffToJson = JSON.stringify(staffList);

    localStorage.setItem("STAFF_LOCAL", staffToJson);

    renderStaff(staffList);
  }
}

function deleteStaff(account) {
  var index = staffList.findIndex(function (staff) {
    return staff.account == account;
  });

  staffList.splice(index, 1);

  var staffToJson = JSON.stringify(staffList);
  localStorage.setItem("STAFF_LOCAL", staffToJson);

  $("#deleteModal").modal("hide");

  renderStaff(staffList);
}

function modifyStaff(account) {
  btnUpdate.style.display = "inherit";
  btnCreate.style.display = "none";

  var alerts = document.querySelectorAll(".sp-thongbao");
  for (var i = 0; i < alerts.length; i++) {
    if (alerts[i] != "") {
      alerts[i].innerHTML = "";
    }
  }

  var index = staffList.findIndex(function (staff) {
    return staff.account == account;
  });

  if (index != -1) {
    showInfo(staffList[index]);
    document.querySelector("#tknv").disabled = true;
  }
}

function updateStaff() {
  var staffModify = getInfo();

  var isValid =
    checkName(staffModify.name) &
    checkEmail(staffModify.email) &
    checkPass(staffModify.pass) &
    checkDayWork(staffModify.dayWork) &
    checkBaseSalary(staffModify.baseSalary) &
    checkPosition(staffModify.position) &
    checkHourWork(staffModify.hourWork);

  if (isValid) {
    var index = staffList.findIndex((staff) => {
      return staff.account == staffModify.account;
    });
    if (index != -1) {
      staffList[index] = staffModify;
      var staffToJson = JSON.stringify(staffList);
      localStorage.setItem("STAFF_LOCAL", staffToJson);

      $("#myModal").modal("hide");

      renderStaff(staffList);
    }
  }
}

function resetForm() {
  document.querySelector("#tknv").disabled = false;
  document.getElementById("staffForm").reset();
}

searchBox.addEventListener("keyup", suggestRating);

function suggestRating() {
  var ratingData = [];
  for (var i = 0; i < staffList.length; i++) {
    var staff = staffList[i];
    var staffRating = staff.rating();
    ratingData.push(staffRating);
  }
  $("#searchName").autocomplete({
    source: ratingData,
  });
}

searchBox.addEventListener("change", searchStaff);

function searchStaff(event) {
  var keyword = event.target.value;
  var matchedList = [];

  var matchedStaff = staffList.filter((staff) => staff.rating().toLowerCase() == keyword.toLowerCase());

  if (matchedStaff.length > 0) {
    for (var i = 0; i < matchedStaff.length; i++) {
      var row = matchedStaff[i];
      var staffLoad = new STAFF(
        row.account,
        row.name,
        row.email,
        row.pass,
        row.dayWork,
        row.baseSalary,
        row.position,
        row.hourWork
      );
      matchedList.push(staffLoad);
      renderStaff(matchedList);
    }
  } else renderStaff(staffList);
}
